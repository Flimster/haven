# Requirements

## A requirements
- [ ] Serve various static things
    - [ ] Static files
    - [ ] Static types (String, json)
- [ ] Router
    - [ ] A Route struct that has a method, route (&str) and a callback to invoke whenever someone goes to that route
## B requirements
- [ ] An easier way to create responses (Response::from(StatusCode::OK, "Hello world"))
    - Should default be html?
- [ ] Development server
    -  [ ] Autoreloading
- [ ] Database access

## C requirements
 - [ ] Dynamic routing
    -  Attribute-like macros

# Architecture

## The Callback Trait
- The Callback trait is used to create responses. The HttpServer stores a vector of Callback trait objects that each can call the _invoke()_ method