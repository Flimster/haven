use crate::router::Router;

use http::method::Method;
use http::request::Request;
use http::response::Response;

use futures::prelude::*;

use runtime::net::{TcpListener, TcpStream};

use std::sync::{Arc, RwLock};

pub struct HttpServer {
    routes: Arc<RwLock<Router>>,
}

impl HttpServer {
    pub fn new() -> HttpServer {
        let routes = Arc::new(RwLock::new(Router::new()));
        HttpServer { routes }
    }

    pub fn new_service<F>(self, method: Method, route: &str, service: F) -> Self
    where
        F: Fn() -> Response + Send + Sync + Clone + 'static,
    {
        self.routes
            .write()
            .expect("Failed to aquire lock to write")
            .new_route(method, route, service);
        self
    }

    pub async fn start(self) {
        let mut listener =
            TcpListener::bind("127.0.0.1:3000").expect("Unable to bind address 127.0.0.1:3000");
        println!("Listening on {}", listener.local_addr().unwrap());

        let callbacks = &self.routes;

        listener
            .incoming()
            .try_for_each_concurrent(None, async move |stream| {
                let callbacks = callbacks.clone();
                runtime::spawn(async move {
                    handle_connection(stream, callbacks).await;
                })
                .await;
                Ok(())
            })
            .await
            .unwrap();
    }
}

async fn handle_connection(mut stream: TcpStream, router: Arc<RwLock<Router>>) {
    println!("Accepting connection from: {}", stream.peer_addr().unwrap());
    let mut buf = [0u8; 4096];

    loop {
        match stream.read(&mut buf).await {
            Ok(0) => {
                println!("Zero bytes read, broken connection");
                break;
            }
            Ok(_) => {
                let s = String::from_utf8_lossy(&buf).into_owned();
                let req = Request::from_string(s);

                let tmp = router
                    .read()
                    .unwrap()
                    .route_doer(req.method(), req.route().as_ref())
                    .to_string();

                stream.write(tmp.as_bytes()).await.unwrap();
                stream.flush().await.unwrap();
            }
            Err(e) => {
                println!("Err: {:?}", e);
            }
        }
    }
}
