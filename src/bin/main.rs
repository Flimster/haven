#![feature(async_await, async_closure)]
extern crate http;

use haven::server::HttpServer;

use http::method::Method;
use http::response::{Response, ResponseBuilder};
use http::status_code::StatusCode;

use std::sync::atomic::{AtomicUsize, Ordering};

static TMP: AtomicUsize = AtomicUsize::new(0);

#[runtime::main]
async fn main() {
    HttpServer::new()
        .new_service(Method::GET, "/", tmp)
        .new_service(Method::GET, "/tmp2", tmp2)
        .start()
        .await;
}

fn tmp() -> Response {
    println!("Hello tmp function");
    let body = String::from("<html><head></head><body>Hello from / route :)</body></html>");
    ResponseBuilder::new(StatusCode::OK)
        .add_header("Content-Length", &body.len().to_string())
        .set_body(body)
        .create_response()
}

fn tmp2() -> Response {
    TMP.fetch_add(1, Ordering::SeqCst);
    println!("{:#?}", TMP);
    let body = String::from("<html><head></head><body>Hello from tmp2 route :)</body></html>");
    ResponseBuilder::new(StatusCode::OK)
        .add_header("Content-Length", &body.len().to_string())
        .set_body(body)
        .create_response()
}
