extern crate http;

use http::response::Response;

/// Handler should be able to call the function that implemenets the Handler trait
pub trait Handler: Send + Sync {
    /// Calls the function that implements handler
    fn handle(&self) -> Response;
}

impl<F> Handler for F
where
    F: Fn() -> Response + Clone + Sync + Send + 'static,
{
    fn handle(&self) -> Response {
        self()
    }
}

#[cfg(test)]
mod handler_tests {
    use super::*;

    use http::response::{Response, ResponseBuilder};
    use http::status_code::StatusCode;

    fn tmp_function() -> Response {
        ResponseBuilder::new(StatusCode::OK).create_response()
    }

    #[test]
    fn handler_test() {
        let respone = tmp_function.handle().to_string();

        assert_eq!("HTTP/1.1 200 OK\r\n\n", respone);
    }

}
