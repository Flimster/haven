use crate::handler::Handler;

use http::method::Method;
use http::response::{Response, ResponseBuilder};
use http::status_code::StatusCode;

use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

/// A Router consits um multiple routes and a default route that is invoked whenever a route is not found
pub struct Router {
    default_route: Box<dyn Handler>,
    routes: HashMap<u64, Route>,
}

impl Router {
    /// Creates a new `Router`
    /// A `Router` has default route when created, but can be overwritten via the `set_default` function
    /// The `Router` has multiple routes 
    pub fn new() -> Router {
        let default_route = Box::new(default);
        let routes = HashMap::new();

        Router {
            default_route,
            routes,
        }
    }

    /// Creates a new route given a `Method`, `&str` and a function that implements the `Handler` trait
    pub fn new_route<F>(&mut self, method: Method, route: &str, handler: F)
    where
        F: Handler + 'static,
    {
        let hash_value = hash_route(method, route);
        let route = Route::new(method, route, handler);
        self.routes.insert(hash_value, route);
    }

    // TODO: Better name
    /// Tries to execute a route at a given path and if no such route exists, then invoke the default_route
    pub fn route_doer(&self, method: Method, route: &str) -> Response {
        let route_id = hash_route(method, route);
        match self.routes.get(&route_id) {
            Some(route) => route.handle(),
            None => self.default_route.handle(),
        }
    }

    // TODO: Implement
    pub fn set_default() {

    }
}

fn default() -> Response {
    let body = String::from("<html><head></head><body>Route not found</body></html>");
    ResponseBuilder::new(StatusCode::OK)
        .add_header("Content-Length", &body.len().to_string())
        .set_body(body)
        .create_response()
}

struct Route {
    method: Method,

    route: String,

    handler: Box<dyn Handler>,
}

impl Route {
    fn new<F: Handler + 'static>(method: Method, route: &str, handler: F) -> Route {
        let route = route.to_string();
        let handler = Box::new(handler);

        Route {
            method,
            route,
            handler,
        }
    }

    fn handle(&self) -> Response {
        self.handler.handle()
    }
}

// * Should we use this function
fn hash_route(method: Method, route: &str) -> u64 {
    let mut hasher = DefaultHasher::default();
    method.hash(&mut hasher);
    route.hash(&mut hasher);
    let value = hasher.finish();
    value
}

#[cfg(test)]
mod router_tests {
    use super::*;
    use http::method::Method;
    use http::response::{Response, ResponseBuilder};
    use http::status_code::StatusCode;

    fn test_function() -> Response {
        ResponseBuilder::new(StatusCode::OK).create_response()
    }

    fn test_function_2() -> Response {
        ResponseBuilder::new(StatusCode::NotFound)
            .set_body(String::from("Content not found"))
            .create_response()
    }

    #[test]
    fn test_router() {
        let mut router = Router::new();

        router.new_route(Method::GET, "/", test_function);
        let first_route_hash_value = hash_route(Method::GET, "/");

        router.new_route(Method::POST, "/", test_function_2);
        let second_route_hash_value = hash_route(Method::POST, "/");

        assert!(router.routes.contains_key(&first_route_hash_value));
        assert!(router.routes.contains_key(&second_route_hash_value));

        assert_eq!(router.routes.len(), 2);

        let route = router.routes.get(&first_route_hash_value).unwrap();
        let response = route.handle().to_string();
        assert_eq!("HTTP/1.1 200 OK\r\n\n", response);

        let route = router.routes.get(&second_route_hash_value).unwrap();
        let response = route.handle().to_string();
        assert_eq!("HTTP/1.1 404 Not Found\r\n\nContent not found", response);
    }
}
